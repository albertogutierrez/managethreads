// ConsoleApplication10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <atomic>
#include <vector>
#include <thread>
#include <mutex>
#include <memory>
#include <windows.h>


using namespace std;
//#define SPFlag



#ifdef SPFlag
class SLock
{
public:
  void lock()
  {
    while (lck.test_and_set(std::memory_order_acquire));
  }

  void unlock()
  {
    lck.clear(std::memory_order_release);
  }

  SLock(){
    //lck = ATOMIC_FLAG_INIT;
    lck.clear();
  }
private:
  std::atomic_flag lck;// = ATOMIC_FLAG_INIT;
};
#elif SPAtomic
class SLock
{
public:
  void lock()
  {
    while (lck.exchange(true));
  }

  void unlock()
  {
    lck = true;
  }

  SLock(){
    //lck = ATOMIC_FLAG_INIT;
    lck = false;
  }
private:
  std::atomic<bool> lck;
};
#elif Mutex
class SLock
{
public:
  void lock()
  {
    lck.lock();
  }

  void unlock()
  {
    lck.unlock();
  }

private:
  std::mutex lck;
};
#else
class SLock
{
public:
  void lock()
  {
    EnterCriticalSection(&g_crit_sec);
  }

  void unlock()
  {
    LeaveCriticalSection(&g_crit_sec);
  }

  SLock(){
    InitializeCriticalSectionAndSpinCount(&g_crit_sec, 0x80000400);
  }

private:
  CRITICAL_SECTION g_crit_sec;
};

#endif






unique_ptr<SLock> raiilock(new SLock());

class Smartlock{
public:
  Smartlock(){ raiilock->lock(); }
  ~Smartlock(){ raiilock->unlock(); }
};




int counter[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

void foo(int n)
{


  Smartlock smlock;
  cout << "Thread number -----------> " << n << endl;
  for (unsigned i = 0; i < 10; i++)
  {

    cout << "Number" << i << endl;

  }

  cout << endl;
}

int main(int, char**)
{
  std::vector<std::thread> v;
  for (int n = 0; n < 8; ++n)
  {
    v.emplace_back(foo, n);
  }

  for (int i = 0; i < 8; ++i)
  {
    v.at(i).join();
  }

 // foo(8);
  unsigned int nthreads = std::thread::hardware_concurrency();

  return 0;
}